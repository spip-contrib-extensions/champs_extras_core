<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/cextras?lang_cible=eo
// ** ne pas modifier le fichier **

return [

	// C
	'cextra_par_defaut' => 'Defaŭlta valoro',

	// P
	'pas_auteur' => 'neniu aŭtoro',

	// T
	'type' => '@type@',

	// Z
	'zbug_balise_argument_non_texte' => 'La argumento @nb@ en la marko @balise@ estu de tipo „teksto“',
];
