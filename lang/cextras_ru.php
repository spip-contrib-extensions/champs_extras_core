<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/cextras?lang_cible=ru
// ** ne pas modifier le fichier **

return [

	// C
	'cextra_par_defaut' => 'Значение по умолчанию',

	// P
	'pas_auteur' => 'автор не указан',

	// T
	'type' => '@type@',

	// Z
	'zbug_balise_argument_non_texte' => 'Параметры @nb@ для тега @balise@ должны быть в текстовом формате',
];
