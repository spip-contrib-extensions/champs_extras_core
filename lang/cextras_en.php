<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/cextras?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// C
	'cextra_par_defaut' => 'Default value',

	// P
	'pas_auteur' => 'no author',

	// T
	'type' => '@type@',

	// Z
	'zbug_balise_argument_non_texte' => 'The @nb@ argument in the tag @balise@ should be text typed',
];
