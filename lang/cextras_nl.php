<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/cextras?lang_cible=nl
// ** ne pas modifier le fichier **

return [

	// C
	'cextra_par_defaut' => 'Standaardwaarde',

	// P
	'pas_auteur' => 'geen auteur',

	// T
	'type' => '@type@',

	// Z
	'zbug_balise_argument_non_texte' => 'Argument @nb@ van het baken @balise@ moet van het type tekst zijn',
];
