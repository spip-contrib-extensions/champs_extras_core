<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/cextras?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// C
	'cextra_par_defaut' => 'Valore predefinito',

	// P
	'pas_auteur' => 'nessun autore',

	// T
	'type' => '@type@',

	// Z
	'zbug_balise_argument_non_texte' => 'L’argomento @nb@ nel tag @balise@ deve essere di tipo testo',
];
