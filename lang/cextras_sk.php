<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/cextras?lang_cible=sk
// ** ne pas modifier le fichier **

return [

	// C
	'cextra_par_defaut' => 'Predvolená hodnota',

	// P
	'pas_auteur' => 'bez autora',

	// T
	'type' => '@type@',

	// Z
	'zbug_balise_argument_non_texte' => 'Parameter @nb@ v tagu @balise@ musí byť typu "text"',
];
