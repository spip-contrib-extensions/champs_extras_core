<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-cextras?lang_cible=es
// ** ne pas modifier le fichier **

return [

	// C
	'cextras_description' => 'Ofrece una API simple permitiendo crear nuevos campos en los objetos editoriales. 
Es pues la base para otros plugins, especialmente para «Campos Extras Interfaz», que da una interfaz gráfica de gestión de estos nuevos campos.',
	'cextras_nom' => 'Campos Extras',
	'cextras_slogan' => 'API de gestión de nuevos campos en los objetos editoriales',
	'cextras_titre' => 'Campos Extras',
];
