<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-cextras?lang_cible=de
// ** ne pas modifier le fichier **

return [

	// C
	'cextras_nom' => 'Zusatzfelder',
	'cextras_slogan' => 'Zusätzliche Felder für die Standardobjekte von SPIP anlegen',
	'cextras_titre' => 'Zusatzfelder',
];
