<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/cextras?lang_cible=de
// ** ne pas modifier le fichier **

return [

	// C
	'cextra_par_defaut' => 'Voreinstellung',

	// P
	'pas_auteur' => 'kein Autor',

	// T
	'type' => '@type@',

	// Z
	'zbug_balise_argument_non_texte' => 'Das Argument @nb@ im Tag @balise@ muß den Typ text haben.',
];
