<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/cextras?lang_cible=ar
// ** ne pas modifier le fichier **

return [

	// C
	'cextra_par_defaut' => 'قيمة تلقائية',

	// T
	'type' => '@type@',
];
